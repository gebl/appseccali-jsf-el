package com.qualcomm.isrm.jsf;

public class LoginBean {
	private String name;
	private String pass;
	private boolean isadmin;

	public boolean isIsadmin() {
		return isadmin;
	}

	public void setIsadmin(boolean isadmin) {
		this.isadmin = isadmin;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(final String password) {
		this.pass = password;
	}
}
